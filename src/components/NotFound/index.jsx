import React from "react";
import {
	createStyles,
	Image,
	Container,
	Title,
	Text,
	Button,
	SimpleGrid,
} from "@mantine/core";
import { Link } from "react-router-dom";

import image from "../../asset/img/image404.svg";

const useStyles = createStyles((theme) => ({
	root: {
		background: "linear-gradient(250deg, rgba(130, 201, 30, 0) 0%, #062343 70%)",
		backgroundPosition: "center",
		width: "100%",
		objectFit: "contain",
		height: "100%",
		position: "absolute",
	},

	container: {
		paddingTop: 80,
		paddingBottom: 80,
	},

	title: {
		fontWeight: 900,
		fontSize: 34,
		marginBottom: theme.spacing.md,
		fontFamily: `Greycliff CF, ${theme.fontFamily}`,

		[theme.fn.smallerThan("sm")]: {
			fontSize: 32,
		},
		color: "#fff",
	},

	control: {
		[theme.fn.smallerThan("sm")]: {
			width: "100%",
		},
	},

	mobileImage: {
		[theme.fn.largerThan("sm")]: {
			display: "none",
		},
	},

	desktopImage: {
		[theme.fn.smallerThan("sm")]: {
			display: "none",
		},
	},
}));

export function NotFound() {
	const { classes } = useStyles();

	return (
		<section className={classes.root}>
			<Container className={classes.container}>
				<SimpleGrid
					spacing={80}
					cols={2}
					breakpoints={[{ maxWidth: "sm", cols: 1, spacing: 40 }]}>
					<Image src={image} className={classes.mobileImage} />
					<div>
						<Title className={classes.title}>Algo no está bien...</Title>
						<Text color='white' size='lg'>
							La página que intenta abrir no existe. Puede que hayas escrito mal el o
							la página se ha movido a otra URL. Si crees que esto es un error
							póngase en contacto con el soporte.
						</Text>
						<Link to='/'>
							<Button
								variant='gradient'
								gradient={{ from: "teal", to: "lime", deg: 105 }}
								size='xl'
								className={classes.control}
								mt={40}>
								Volver a la pagina de Inicio
							</Button>
						</Link>
					</div>
					<Image src={image} className={classes.desktopImage} />
				</SimpleGrid>
			</Container>
		</section>
	);
}
