import React from "react";
import { UnstyledButton, Group, Avatar, Text, createStyles } from "@mantine/core";
import { ChevronRight } from "tabler-icons-react";
import { NavLink } from "react-router-dom";

const useStyles = createStyles((theme) => ({
	user: {
		display: "block",
		width: "100%",
		padding: theme.spacing.md,
		color: theme.colorScheme === "dark" ? theme.colors.dark[0] : theme.black,

		"&:hover": {
			backgroundColor:
				theme.colorScheme === "dark" ? theme.colors.dark[8] : theme.colors.gray[0],
		},
	},
}));

export function UserButton({ image, name, lastName, id, icon, link, ...others }) {
	const { classes } = useStyles();

	return (
		<UnstyledButton className={classes.user} {...others}>
			<NavLink
				style={({ isActive }) => ({
					color: isActive ? "orange" : "gray",
					textDecoration: "none",
				})}
				to={`/${link}/${id}`}
				key={id}>
				<Group>
					<Avatar color='cyan' radius='xl'>
						{`${name.substr(0, 1)}${lastName.substr(0, 1)}`}
					</Avatar>
					<div style={{ flex: 1 }}>
						<Text size='lg' weight={500}>
							{`${name} ${lastName}`}
						</Text>
					</div>

					{icon || <ChevronRight size={14} />}
				</Group>
			</NavLink>
		</UnstyledButton>
	);
}
