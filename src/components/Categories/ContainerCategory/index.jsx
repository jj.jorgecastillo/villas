import {
	Container,
	Center,
	Loader,
	Grid,
	Card,
	Space,
	Badge,
	Code,
} from "@mantine/core";

import { Calendar } from "tabler-icons-react";

export function ContainerCategory({ data, label }) {
	return (
		<Container my='md'>
			<Center>
				{!data.length ? (
					<Loader color='grape' size='xl' variant='dots' />
				) : (
					<Grid gutter='xs' grow>
						{data.map(({ mes, semana }) => {
							const { martes, sabados } = semana;
							return (
								<Grid.Col key={mes}>
									{martes.length || sabados.length ? (
										<Card
											shadow='xl'
											p='sm'
											m='xs'
											style={{ width: "100%" }}
											radius='sm'>
											<Card.Section>
												<Center mb='xs'>
													<Badge
														p='xs'
														size='lg'
														radius='xs'
														color='teal'
														leftSection={
															<Calendar style={{ margin: "6px 0 0" }} size={16} />
														}
														sx={(theme) => ({
															backgroundColor: theme.colors.green[0],
															"&:hover": {
																backgroundColor: theme.colors.green[1],
															},
														})}>
														{`${mes} | ${label}`}
													</Badge>
												</Center>
											</Card.Section>

											{semana.martes.length ? (
												<div>
													<Badge radius='xs' color='violet'>
														Martes
													</Badge>
													<Space />
													{martes.map((dia) => (
														<Code style={{ margin: "0 0 4px" }}>{dia}</Code>
													))}
												</div>
											) : null}

											<Space h='xs' />
											{semana.sabados.length ? (
												<div>
													<Badge radius='xs' color='orange'>
														Sábados
													</Badge>
													<Space />
													{sabados.map((dia) => (
														<Code style={{ margin: "0 0 4px" }}>{dia}</Code>
													))}
												</div>
											) : null}

											<Space h='xs' />
										</Card>
									) : null}
								</Grid.Col>
							);
						})}
					</Grid>
				)}
			</Center>
		</Container>
	);
}
