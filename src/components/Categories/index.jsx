import { Tabs } from "@mantine/core";
import { Microphone2, Trash, UserCheck, DeviceLaptop } from "tabler-icons-react";
import { AffixTop } from "../AffixTop";
import { ContainerCategory } from "./ContainerCategory";

const { Tab } = Tabs;
const propsTab = {
	size: 30,
	strokeWidth: 2,
	color: "#bc40bf",
};

export function Categories({ user }) {
	return (
		<Tabs color='green' defaultValue='Mantenimiento'>
			<Tab label='Acomodador' icon={<UserCheck {...propsTab} />}>
				<ContainerCategory data={user?.acomodador || []} label='Acomodador' />
			</Tab>
			<Tab label='Audio y Video' icon={<DeviceLaptop {...propsTab} />}>
				<ContainerCategory data={user?.audiovideo || []} label='Audio y Video' />
			</Tab>
			<Tab label='Mantenimiento' icon={<Trash {...propsTab} />}>
				<ContainerCategory data={user?.mantenimiento || []} label='Mantenimiento' />
			</Tab>
			<Tab label='Micrófono' icon={<Microphone2 {...propsTab} />}>
				<ContainerCategory data={user?.microfono || []} label='Micrófono' />
			</Tab>
			<AffixTop />
		</Tabs>
	);
}
