import "dayjs/locale/es";
import { DatePicker } from "@mantine/dates";
import { useState } from "react";
import { useFormik } from "formik";
import { useParams } from "react-router-dom";
import { getUser } from "../../data/data";
import {
	Avatar,
	TextInput,
	Button,
	Box,
	Group,
	Container,
	Divider,
	Badge,
	Text,
	Card,
	Space,
	Collapse,
} from "@mantine/core";

import { Calendar } from "tabler-icons-react";

const textDecorator = ({ label, from = "teal", to = "lime" }) => (
	<Text
		component='span'
		inherit
		variant='gradient'
		gradient={{ from: from, to: to, deg: 105 }}
		style={{ fontSize: 16 }}>
		{label}
	</Text>
);

const AssignationForm = () => {
	let params = useParams();
	let user = getUser(params.id);
	const [opened, setOpen] = useState(false);

	const { name, lastName, acomodador } = user;

	const avatar = (
		<Avatar color='cyan' radius='xl'>
			{`${name.substr(0, 1).toUpperCase()}${lastName.substr(0, 1).toUpperCase()}`}
		</Avatar>
	);

	const formik = useFormik({
		initialValues: {
			name: user.name,
			lastName: user.lastName,
			acomodador:
				acomodador.map(({ semana }) =>
					semana.map(({ fechas }) =>
						fechas.map(({ fecha }) => {
							return fecha;
						})
					)
				) || [],
		},
		enableReinitialize: true,
		onSubmit: (values) => {
			// alert(JSON.stringify(values, null, 2));
			console.log(values);
		},
	});

	const handleChangeAcomodador = ({ e, values }) => {
		console.log(e);
		console.log(values);
		formik.handleChange = e;
	};

	return (
		<Container>
			<Divider
				my='xl'
				size='md'
				label={
					<>
						<Badge
							sx={{ paddingLeft: 0 }}
							size='lg'
							radius='xl'
							color='teal'
							leftSection={avatar}>
							{name} {lastName}
						</Badge>
					</>
				}
				labelPosition='center'
			/>
			<Box sx={{ maxWidth: 340 }} mx='auto'>
				<form onSubmit={formik.handleSubmit}>
					<TextInput
						required
						id='name'
						label={textDecorator({ label: "Nombre" })}
						placeholder='Juan'
						onChange={formik.handleChange}
						value={formik.values.name}
					/>
					<TextInput
						id='lastName'
						required
						label={textDecorator({ label: "Apellido" })}
						placeholder='Perez'
						mt='sm'
						onChange={formik.handleChange}
						value={formik.values.lastName}
					/>
					<Space h='md' />
					{/* <Button onClick={() => setOpen((o) => !o)}>Acomodador</Button>
					<Collapse in={opened}> */}
					{acomodador.map(({ mes, semana }) => {
						return (
							<div key={mes}>
								<Divider
									mt='xs'
									label={
										<Box ml={5}>
											{textDecorator({
												label: mes,
												from: "orange",
												to: "yellow",
											})}
										</Box>
									}
									labelPosition='center'
								/>
								{semana.map(({ dia, fechas }, indexSemana) => {
									return (
										<div>
											<Space h='xs' />
											<Card key={indexSemana} radius='md'>
												{textDecorator({ label: dia })}
												{fechas.map(({}, indexDia) => {
													return (
														<>
															<Space h='xs' />
															<DatePicker
																id='acomodador'
																locale='es'
																placeholder='Fecha'
																icon={<Calendar size={16} />}
																required
																onChange={(e) =>
																	handleChangeAcomodador({
																		e,
																		values:
																			formik.values.acomodador[indexSemana][
																				indexSemana
																			][indexDia],
																	})
																}
																defaultValue={
																	new Date(
																		formik.values.acomodador[indexSemana][
																			indexSemana
																		][indexDia]
																	)
																}
															/>
														</>
													);
												})}
											</Card>
										</div>
									);
								})}
							</div>
						);
					})}
					{/* </Collapse> */}

					<Group position='right' mt='xl'>
						<Button
							variant='gradient'
							type='submit'
							gradient={{ from: "teal", to: "indigo", deg: 105 }}>
							Editar
						</Button>
					</Group>
				</form>
			</Box>
		</Container>
	);
};

export default AssignationForm;
