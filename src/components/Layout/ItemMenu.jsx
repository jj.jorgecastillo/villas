import { Navbar, List, ThemeIcon, Text } from "@mantine/core";
import { NavLink } from "react-router-dom";
const { Section } = Navbar;
const { Item } = List;

const ItemMenu = ({ link, color, icon, label }) => {
	return (
		<Section m='xs'>
			<NavLink
				style={({ isActive }) => ({
					color: isActive ? "orange" : "gray",
					textDecoration: "none",
				})}
				to={`/${link}`}>
				<Item
					icon={
						<ThemeIcon color={color} size={30} radius='xl'>
							{icon}
						</ThemeIcon>
					}>
					<Text
						variant='gradient'
						gradient={{ from: "indigo", to: "cyan", deg: 45 }}>
						{label}
					</Text>
				</Item>
			</NavLink>
		</Section>
	);
};

export default ItemMenu;
