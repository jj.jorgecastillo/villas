import React from "react";
import {
	Container,
	Grid,
	SimpleGrid,
	Skeleton,
	useMantineTheme,
} from "@mantine/core";

const PRIMARY_COL_HEIGHT = 300;

export function Layout({ navbar, outlet }) {
	const theme = useMantineTheme();
	const SECONDARY_COL_HEIGHT = PRIMARY_COL_HEIGHT / 2 - theme.spacing.md / 2;

	return (
		<Container my='xs'>
			<SimpleGrid cols={2} spacing='md' breakpoints={[{ maxWidth: "sm", cols: 1 }]}>
				{navbar ?? (
					<Skeleton height={PRIMARY_COL_HEIGHT} radius='md' animate={false} />
				)}

				<Grid gutter='md'>
					<Grid.Col>
						{outlet ?? (
							<Skeleton height={SECONDARY_COL_HEIGHT} radius='md' animate={false} />
						)}
					</Grid.Col>
				</Grid>
			</SimpleGrid>
		</Container>
	);
}
