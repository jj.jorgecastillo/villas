import {
	ListDetails,
	Printer,
	UserExclamation,
	UserMinus,
	UserPlus,
} from "tabler-icons-react";

const dataMenu = [
	{
		link: "assignations",
		icon: <ListDetails size={16} />,
		label: "Asignaciones",
		color: "teal",
	},
	{
		link: "printer-assignation",
		icon: <Printer size={16} />,
		label: "Imprimir Asignaciones",
		color: "indigo",
	},
	{
		link: "add-users",
		icon: <UserPlus size={16} />,
		label: "Agregar Encargado",
		color: "teal",
	},
	{
		link: "edit-users",
		icon: <UserExclamation size={16} />,
		label: "Editar Encargado",
		color: "blue",
	},
	{
		link: "delete-user",
		icon: <UserMinus size={16} />,
		label: "Eliminar Encargado",
		color: "red",
	},
];

export default dataMenu;
