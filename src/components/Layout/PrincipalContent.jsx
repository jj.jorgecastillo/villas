import { AppShell, Header, Center, Text, Image } from "@mantine/core";

import logo from "../../asset/img/logo.png";

import NavBarMenu from "./NavBarMenu";

export default function AppShellDemo({ children }) {
	return (
		<AppShell
			padding='md'
			navbar={<NavBarMenu />}
			header={
				<Header height={60} p='xs'>
					<Center>
						<Image src={logo} mr='xs' width='40px' alt='logo arreglos mecánicos' />
						<Text
							variant='gradient'
							gradient={{ from: "indigo", to: "cyan", deg: 45 }}
							size='xl'
							weight={700}
							style={{ fontFamily: "Greycliff CF, sans-serif" }}>
							Arreglos Mecánicos
						</Text>
					</Center>
				</Header>
			}
			styles={(theme) => ({
				main: {
					backgroundColor:
						theme.colorScheme === "dark"
							? theme.colors.dark[8]
							: theme.colors.gray[0],
				},
			})}>
			{children}
		</AppShell>
	);
}
