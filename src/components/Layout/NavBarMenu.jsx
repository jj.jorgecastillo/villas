import { Navbar, Center, Button } from "@mantine/core";
import { Link } from "react-router-dom";

import dataMenu from "./dataMenu";
import ItemMenu from "./ItemMenu";

const { Section } = Navbar;

const NavBarMenu = () => {
	return (
		<Navbar width={{ base: 300 }} height='auto' p='xs'>
			{dataMenu?.map(({ link, color, icon, label }) => (
				<ItemMenu key={link} link={link} color={color} icon={icon} label={label} />
			))}
			<Center>
				<Section mt='md'>
					<Link to='/'>
						<Button
							variant='gradient'
							gradient={{ from: "teal", to: "lime", deg: 105 }}>
							Salir
						</Button>
					</Link>
				</Section>
			</Center>
		</Navbar>
	);
};

export default NavBarMenu;
