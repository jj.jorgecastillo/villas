import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { MantineProvider, Container, Center, Text } from "@mantine/core";

import App from "./App";
import Assignations from "./routes/Assignations";
import Assignation from "./routes/Assignation";

import { NotFound } from "./components/NotFound";
import PrincipalContent from "./components/Layout/PrincipalContent";

import "./index.css";
import EditUsers from "./routes/EditUsers";
import AssignationForm from "./components/AssignationForm";

ReactDOM.render(
	<MantineProvider
		theme={{
			// Override any other properties from default theme
			fontFamily: "Roboto, sans-serif",
			spacing: { xs: 15, sm: 20, md: 25, lg: 30, xl: 40 },
			colorScheme: "dark",
		}}>
		<BrowserRouter>
			<Routes>
				<Route path='/' element={<App />} />
				<Route
					path='edit-users'
					element={
						<PrincipalContent>
							<EditUsers />
						</PrincipalContent>
					}>
					<Route
						index
						element={
							<Container size={600} px={0}>
								<Center>
									<Text
										component='span'
										align='center'
										variant='gradient'
										gradient={{ from: "indigo", to: "cyan", deg: 45 }}
										size='xl'
										weight={700}
										style={{ fontFamily: "Greycliff CF, sans-serif" }}>
										Debes seleccionar un encargado para Editarlo.
									</Text>
								</Center>
							</Container>
						}
					/>
					<Route path=':id' element={<AssignationForm />} />
				</Route>
				<Route
					path='assignations'
					element={
						<PrincipalContent>
							<Assignations />
						</PrincipalContent>
					}>
					<Route
						index
						element={
							<Container size={600} px={0}>
								<Center>
									<Text
										component='span'
										align='center'
										variant='gradient'
										gradient={{ from: "indigo", to: "cyan", deg: 45 }}
										size='xl'
										weight={700}
										style={{ fontFamily: "Greycliff CF, sans-serif" }}>
										Debes seleccionar un encargado para ver los detalles de sus
										asignaciones.
									</Text>
								</Center>
							</Container>
						}
					/>
					<Route path=':id' element={<Assignation />} />
				</Route>
				<Route path='*' element={<NotFound />} />
			</Routes>
		</BrowserRouter>
	</MantineProvider>,
	document.getElementById("root")
);
