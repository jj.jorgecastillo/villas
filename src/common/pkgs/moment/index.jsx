import moment from "moment";

// pre config
moment.locale("es");
moment.defaultFormatUtc = moment.defaultFormat;

exports = moment;
