import {
	Divider,
	Input,
	InputWrapper,
	Navbar,
	ScrollArea,
	Text,
} from "@mantine/core";
import { UserSearch } from "tabler-icons-react";
import { Outlet, useSearchParams } from "react-router-dom";
import { getUsers } from "../data/data";
import { UserButton } from "../components/UserSmall";
import { Layout } from "../components/Layout";

export default function EditUsers() {
	let users = getUsers();
	let [searchParams, setSearchParams] = useSearchParams();

	return (
		<Layout
			outlet={<Outlet />}
			navbar={
				<Navbar height='80vh' p='xs' width={{ base: 400 }}>
					<Navbar.Section mt='xs' mb='xs'>
						{
							<InputWrapper
								id='edit-user'
								required
								label={
									<Text
										component='span'
										align='center'
										variant='gradient'
										gradient={{ from: "indigo", to: "cyan", deg: 45 }}
										size='xl'
										weight={700}
										style={{ fontFamily: "Greycliff CF, sans-serif" }}>
										Editar Encargados
									</Text>
								}
								description='Por favor verifique los datos de sus encargados antes de Editarlos'
								size='xl'>
								<Divider my='xs' size='md' />
								<Input
									id='edit-user'
									icon={<UserSearch size={48} strokeWidth={2} color={"#4044bf"} />}
									variant='filled'
									placeholder='Buscar Encargado'
									value={searchParams.get("filter") || ""}
									onChange={(event) => {
										let filter = event.target.value;
										if (filter) {
											setSearchParams({ filter });
										} else {
											setSearchParams({});
										}
									}}
								/>
							</InputWrapper>
						}
					</Navbar.Section>

					<Navbar.Section grow component={ScrollArea} mx='-xs' px='xs'>
						{users
							.filter((user) => {
								let filter = searchParams.get("filter");
								if (!filter) return true;
								let name = user.name.toLowerCase();
								return name.startsWith(filter.toLowerCase());
							})
							.map((user) => (
								<UserButton
									link='edit-users'
									id={user.id}
									name={user.name}
									lastName={user.lastName}
								/>
							))}
					</Navbar.Section>
				</Navbar>
			}
		/>
	);
}
