import { useParams } from "react-router-dom";
import { getUser } from "../data/data";
import { Container, Badge, Avatar, Divider } from "@mantine/core";
import { Categories } from "../components/Categories";

export default function Assignation() {
	let params = useParams();
	let user = getUser(params.id, 10);
	const avatar = (
		<Avatar color='cyan' radius='xl'>
			{`${user.name.substr(0, 1).toUpperCase()}${user.lastName
				.substr(0, 1)
				.toUpperCase()}`}
		</Avatar>
	);

	return (
		<Container>
			<Divider
				my='xl'
				size='md'
				label={
					<>
						<Badge
							sx={{ paddingLeft: 0 }}
							size='lg'
							radius='xl'
							color='teal'
							leftSection={avatar}>
							{user.name} {user.lastName}
						</Badge>
					</>
				}
				labelPosition='center'
			/>
			<Categories user={user} />
		</Container>
	);
}
