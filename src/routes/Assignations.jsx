import {
	Divider,
	Input,
	InputWrapper,
	Navbar,
	ScrollArea,
	Text,
} from "@mantine/core";
import { UserSearch } from "tabler-icons-react";
import { Outlet, useSearchParams, Link } from "react-router-dom";
import { getUsers } from "../data/data";
import { UserButton } from "../components/UserSmall";
import { Layout } from "../components/Layout";

export default function Assignation() {
	let users = getUsers();
	let [searchParams, setSearchParams] = useSearchParams();

	return (
		<Layout
			outlet={<Outlet />}
			navbar={
				<Navbar
					p='sm'
					width={{ base: 400 }}
					style={{
						borderRadius: 8,
						boxShadow: "-3px 7px 7px -1px rgba(00,00,10,0.50)",
					}}>
					<Navbar.Section>
						{
							<InputWrapper
								id='list-assignation'
								required
								label={
									<Text
										component='span'
										align='center'
										variant='gradient'
										gradient={{ from: "indigo", to: "cyan", deg: 45 }}
										size='xl'
										weight={700}
										style={{ fontFamily: "Greycliff CF, sans-serif" }}>
										Encargados
									</Text>
								}
								description='Por favor verifique sus asignaciones pendientes'
								size='xl'>
								<Divider my='xs' size='md' />
								<Input
									id='list-assignation'
									icon={<UserSearch size={48} strokeWidth={2} color={"#4044bf"} />}
									variant='filled'
									placeholder='Buscar Encargado'
									value={searchParams.get("filter") || ""}
									onChange={(event) => {
										let filter = event.target.value;
										if (filter) {
											setSearchParams({ filter });
										} else {
											setSearchParams({});
										}
									}}
								/>
							</InputWrapper>
						}
					</Navbar.Section>

					<Navbar.Section grow component={ScrollArea} mx='-xs' px='xs'>
						{users
							.filter((user) => {
								let filter = searchParams.get("filter");
								if (!filter) return true;
								let name = user.name.toLowerCase();
								return name.startsWith(filter.toLowerCase());
							})
							.map((user) => (
								<UserButton
									link='assignations'
									id={user.id}
									name={user.name}
									lastName={user.lastName}
								/>
							))}
					</Navbar.Section>
				</Navbar>
			}
		/>
	);
}
