// import logo from "./asset/img/logo.png";
// import { AspectRatio, Image } from "@mantine/core";

// import "./App.css";

// function App() {
// 	return (
// 		<div className='App'>
// 			<header className='App-header'>
// 				<p>Bienvenido a los Arreglos Mecánicos</p>
// 				<p>
// 					Por favor recuerda comunicarte con tiempo si no puedes cumplir con la
// 					asignación que te corresponde.
// 				</p>
// 			</header>

// 			<AspectRatio ratio={720 / 1080} sx={{ maxWidth: 300 }} mx='auto'>
// 				<Link to='/asignaciones'>
// 					<Image src={logo} alt='Portada' />
// 				</Link>
// 			</AspectRatio>
// 		</div>

// 	);
// }

import React from "react";
import { Link } from "react-router-dom";
import { createStyles, Container, Title, Text, Button } from "@mantine/core";

import p1 from "./asset/img/p1.jpg";
import p2 from "./asset/img/p2.jpg";
import p3 from "./asset/img/p3.jpg";
import p4 from "./asset/img/p4.jpg";
import p5 from "./asset/img/p5.jpg";

import "./App.css";

const Background = { 1: p1, 2: p2, 3: p3, 4: p4, 5: p5 };

const getRandomInt = (min, max) => {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
};

const RandomNumber = getRandomInt(1, 5);

const useStyles = createStyles((theme) => ({
	root: {
		position: "absolute",
		backgroundColor: "#11284b",
		backgroundSize: "cover",
		backgroundPosition: "center center",
		backgroundImage: `linear-gradient(250deg, rgba(130, 201, 30, 0) 0%, #062343 100%), url(${Background[RandomNumber]})`,
		backgroundRepeat: "no-repeat",
		backgroundAttachment: "fixed",
		overflow: "hidden",
		width: "100%",
		objectFit: "cover",
		height: "100%",
	},

	inner: {
		display: "flex",
		justifyContent: "space-between",

		[theme.fn.smallerThan("md")]: {
			flexDirection: "column",
		},
	},

	image: {
		[theme.fn.smallerThan("md")]: {
			display: "none",
		},
	},

	content: {
		paddingTop: theme.spacing.xl * 2,
		paddingBottom: theme.spacing.xl * 2,
		marginRight: theme.spacing.xl * 3,

		[theme.fn.smallerThan("md")]: {
			marginRight: 0,
		},
	},

	title: {
		color: theme.white,
		fontFamily: `Greycliff CF, ${theme.fontFamily}`,
		fontWeight: 900,
		lineHeight: 1.05,
		maxWidth: 500,
		fontSize: 48,

		[theme.fn.smallerThan("md")]: {
			maxWidth: "100%",
			fontSize: 34,
			lineHeight: 1.15,
		},
	},

	description: {
		color: theme.white,
		opacity: 0.75,
		maxWidth: 500,

		[theme.fn.smallerThan("md")]: {
			maxWidth: "100%",
		},
	},

	control: {
		paddingLeft: 50,
		paddingRight: 50,
		fontFamily: `Greycliff CF, ${theme.fontFamily}`,
		fontSize: 22,

		[theme.fn.smallerThan("md")]: {
			width: "100%",
		},
	},
}));

export function App() {
	const { classes } = useStyles();
	return (
		<div className={classes.root}>
			<Container size='lg'>
				<div className={classes.inner}>
					<div className={classes.content}>
						<Title className={classes.title}>
							Conoce{" "}
							<Text
								component='span'
								inherit
								variant='gradient'
								gradient={{ from: "teal", to: "lime", deg: 105 }}>
								Tus Asignaciones
							</Text>{" "}
						</Title>

						<Text className={classes.description} mt={30}>
							En los arreglos mecánicos que te corresponen, recuerda avisar si no
							puedes cumplir con la asignación que te corresponde.
						</Text>
						<Link to='/assignations'>
							<Button
								variant='gradient'
								gradient={{ from: "teal", to: "lime", deg: 105 }}
								size='xl'
								className={classes.control}
								mt={40}>
								Comenzar
							</Button>
						</Link>
					</div>
				</div>
			</Container>
		</div>
	);
}

export default App;
