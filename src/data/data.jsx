let users = [
	{
		id: "01",
		name: "Jorge",
		lastName: "Castillo",
		grupo: "Grupo 1",
		acomodador: [
			{
				mes: "Mayo",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "05-01-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
			{
				mes: "Junio",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "01-05-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
		],
		audiovideo: [
			{
				mes: "Mayo",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "01-05-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
			{
				mes: "Junio",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "01-05-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
		],
		mantenimiento: [
			{
				mes: "Mayo",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "01-05-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
			{
				mes: "Junio",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "01-05-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
		],
		microfono: [
			{
				mes: "Mayo",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "01-05-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
			{
				mes: "Junio",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "01-05-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
		],
	},
	{
		id: "02",
		name: "Jesé",
		lastName: "Castillo",
		grupo: "Grupo 1",
		acomodador: [
			{
				mes: "Mayo",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "01-05-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
			{
				mes: "Junio",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "01-05-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
		],
		audiovideo: [
			{
				mes: "Mayo",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "01-05-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
			{
				mes: "Junio",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "01-05-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
		],
		mantenimiento: [
			{
				mes: "Mayo",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "01-05-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
			{
				mes: "Junio",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "01-05-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
		],
		microfono: [
			{
				mes: "Mayo",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "01-05-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
			{
				mes: "Junio",
				semana: [
					{
						dia: "martes",
						fechas: [{ fecha: "01-05-2020" }, { fecha: "02-05-2020" }],
					},
					{
						dia: "Sábados",
						fechas: [{ fecha: "03-05-2022" }, { fecha: "04-05-2022" }],
					},
				],
			},
		],
	},
];

export function getUsers() {
	return users;
}

export function getUser(id) {
	return users.find((user) => user.id === id);
}

export function deleteuser(id) {
	users = users.filter((user) => user.id !== id);
}
